class Product < ActiveRecord::Base
	validates :title , :description , :image_url , presence: true
	validates :price , numericality: { greater_than_or_equal: 0.01}
	validates :title , uniqueness: true 
	validates :image_url , allow_blank: true , format: {
         with: %r{\.(git|jpg|png)\Z}i,
      message: 'must be a URL for GIF, jpg or PNG image.'
	}
 
  has_many :line_items
  before_destroy :ensure_not_referenced_by_any_line_item


# make sure the product we are selecting is not already present in the linItem
# Remember : Line item # => #belongs_to :product  #belongs_to :cart
  def ensure_not_referenced_by_any_line_item
     if line_items.empty?
     	return true 
     else 
     	errors.add(:base, 'Line Items present')
     	return false
     end
  end
  

  # returns latest product for caching purposes 
  def self.latest 

    Product.order(:updated_at).last

  end


end
