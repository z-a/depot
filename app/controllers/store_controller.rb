class StoreController < ApplicationController

  
  #after creating the cart partial , we were unable to acess it . Hence , we are invoking the layout while looking at the store index action. 
  # and that action does not currently set @cart... so we will add the below which will make the partail available .. hopefully. 
 

  include CurrentCart
  before_action :set_cart


  #uncomment once is ready for deployment.    
  #before_action :authenticate_user!
 
  def index
  # here we need to get the product out of the database and dislay in the store
  # ask the model for  list of products that we can sell.
  @products = Product.order(:title)

  end





end 

