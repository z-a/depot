module CurrentCart
	extend ActiveSupport::Concern

	private
    # rails makes current session look like a hash to the controller , so we will store the ID of the 
    # current cart in the session by indexing it with the symbol :cart_id
    # so , the set_car method starts by getting the :cart_id from the sesssion object
    # and then attempt to find the cart corresponding to this ID
    # if such a cart is not found, then this method will create a new Cart , store the ID of the created cart into a session
    # and return the new cart. 
    #
    # ==>> read more about sessions .. http://guides.rubyonrails.org/security.html

    def set_cart
			@cart = Cart.find(session[:cart_id])
			rescue ActiveRecord::RecordNotFound
			@cart = Cart.create
			session[:cart_id] = @cart.id
    end
 
end
